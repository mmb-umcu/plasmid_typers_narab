# Plasmid Typing Visualization

## Overview

plasmid_typers is a comprehensive collection of tools to visualize plasmid metadata.
The pipeline will enable users to extract
information related to plasmid type, mobility, resistance genes, and
plasmid mge-cluster and visualize it in a shiny app.

## Installation

Clone the repository and change directory

    git clone https://gitlab.com/mmb-umcu/plasmid_typers_narab.git
    cd plasmid_typers_narab

It contains data of predicted E.faecium plasmids that contain NarAB genes and indicates their mge-cluster, mobility, ARGs and VAGs, and plasmid replicon/Inc-group.

The shiny app visualises this data in a user-friendly way. 

#### Launching the App

To launch the visualization app you can run:

```
Rscript app/app.R
``` 

It expects a metadata file (metadata.txt) in 


    /plasmid_typers/output/
       ├── metadata.txt
       └── ...

The metadata.txt file is a ';' delimited text file that expects header as indicated in the example file in the same directory.

The sequences of the plasmids that were used for visualization can be found in input/

#### Contributions

This pipeline and shiny app is a collaborative project of students of the HU (Thijmen Weijgertze, Dian Dupon, Lamyae Elmahboud, Anne Bakker, Anna van Donkersgoed, Twan Samson), the RIVM  (Gijs Teunis) and the UMCU. Prediction of narAB plasmid has been done by Jesse Kerkvliet and Anita Schurch.

 
